# My first Quarkus project - rest-book
This project is my first experiment with Quarkus, it contains:
- a simple REST controller
- a service which convert entities to DTOs using the library MapStruct
- a repository that mock the database
- integration tests

# Dependencies
To run this project you have to install:
- Java 11 
- GraalVM
- An IDE (I have used IntellJ)
- Maven
- Docker (if you want to build an image to run in a docker container)
- A software to trigger APIs like Postman (or you can just use cURL)

## Useful notes

# Start the application:
    - start in development mode
        - mvn quarkus:dev
        - [started in 3.938s]
    - Building Executable JARs and execute it, this is the default jar type:
        - mvn package -DskipTests
        - cd target/
        - ls (see rest-book-1.0.0-SNAPSHOT.jar)
        - cd .\quarkus-app\
        - ls (see quarkus-run.jar, we need to execute this, this jar is really tiny because it just contains the code to boostrap the entire structure)
        - java -jar .\quarkus-run.jar (to start the application, it says "Profile prod activated")
        - [started in 1.679s]
        - we can test it with "curl http://localhost:8080/api/books" and we will se logged "[PROD] - Returns all books"
    - Building Executable Uber JARs (also called fat-jar) and execute it:
        - package -DskipTests -Dquarkus.package.type=uber-jar
        - ...
        - now the "rest-book-1.0.0-SNAPSHOT-runner.jar" is bigger because it contains all our dependencies
        - java -jar .\quarkus-run.jar
        - [started in 1.917s]
    - Building Native Executable JARs with GraalVM, previous Executable JARs contains bytecode so to run it we need a JVM, we trigger first the JVM passing
      a JAR file and the JVM does bytecode interpretation and execute our application, BUT native images do not contain bytecode, they contain binary and these
      JAR are very small because GraalVM tries to get only the code that your application uses, it doesn't include unused class and also unused method inside
      classes, unfortunately this type of compilation is really resource intensive
        - package -Dquarkus.package.type=native (this will take a few minutes)
        - ...
        - now the "rest-book-1.0.0-SNAPSHOT-runner*" is an executable file, a blob
        - ./target/rest-book-1.0.0-SNAPSHOT-runner (I only need this to execute it)
        - [started in 0.027s]
        - sometimes GraalVM will get rid of code that it doesn't understand how it enhances the code, producing a binary with function that don't work, so it's
          a good idea to execute the tests that we have against the binary, we can do this creating a class test with the annotation @QuarkusIntegrationTest
          and make extends this class by another class that contains the test that we want to run in native mode, we can also add others tests or override them,
          in this way quarkus builds native executable first and then run the test suite, to execute these test we need to use "mvn verify -Pnative".
          To enable these tests we need to add in pom.xml the profile name "native":
              <id>native</id>
              <activation>
                <property>
                  <name>native</name>
                </property>
              </activation>
          and the failsafe correct plugin:
              <plugin>
                <artifactId>maven-failsafe-plugin</artifactId>
                <version>${surefire-plugin.version}</version>
                <executions>
                  <execution>
                    <goals>
                      <goal>integration-test</goal>
                      <goal>verify</goal>
                    </goals>
                    <configuration>
                      <systemPropertyVariables>
                        <native.image.path>${project.build.directory}/${project.build.finalName}-runner</native.image.path>
                        <java.util.logging.manager>org.jboss.logmanager.LogManager</java.util.logging.manager>
                        <maven.home>${maven.home}</maven.home>
                      </systemPropertyVariables>
                    </configuration>
                  </execution>
                </executions>
              </plugin>

------------------------------------------------------------------------------      

# Swagger

You can acces to swagger from: http://localhost:8080/q/swagger-ui/

------------------------------------------------------------------------------

# The following text is generated by the Quarkus project generator

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application, packaged as an _über-jar_, is now runnable using `java -jar target/*-runner.jar`.

## Creating a native executable

You can create a native executable using: 
```shell script
./mvnw package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 
```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/rest-book-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.

## Related Guides

- RESTEasy Classic JSON-B ([guide](https://quarkus.io/guides/rest-json)): JSON-B serialization support for RESTEasy Classic

## Provided Code

### RESTEasy JAX-RS

Easily start your RESTful Web Services

[Related guide section...](https://quarkus.io/guides/getting-started#the-jax-rs-resources)
