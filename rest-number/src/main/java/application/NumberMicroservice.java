package application;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;
import org.eclipse.microprofile.openapi.annotations.ExternalDocumentation;
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

@ApplicationPath("/")
@OpenAPIDefinition(
        info = @Info(
                title = "Number Microservice",
                description = "This microservice generates ISBN book numbers",
                version = "1.0",
                contact = @Contact(name = "@MarcoCordoni", url = "https://gitlab.com/marco.cordoni")
        ),
        externalDocs = @ExternalDocumentation(
                url = "https://gitlab.com/marco.cordoni",
                description = "My GitLab repository"
        ),
        tags = {
                @Tag(
                        name = "api",
                        description = "Public API to generate ISBN numbers"
                ),
                @Tag(
                        name = "numbers",
                        description = "Interested in numbers"
                ),
        }
)
public class NumberMicroservice extends Application {

}
