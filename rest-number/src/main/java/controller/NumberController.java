package controller;

import dto.IsbnNumbers;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.logging.Logger;
import service.NumberService;

import javax.naming.Name;

@Path("/api/numbers")
@Tag(name = "Number REST endpoint")
public class NumberController {

    @Inject
    private Logger logger;

    @Inject
    private NumberService numberService;

    @ConfigProperty(name = "env", defaultValue = "unknown")
    private String env;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
            summary = "Generates book numbers",
            description = "ISBN 13 and 10"
    )
    public IsbnNumbers generateIsbnNumbers() {
        IsbnNumbers isbnNumbers = numberService.generateIsbnNumbers();
        logger.infof("Numbers generated: %s", isbnNumbers);
        return isbnNumbers;
    }

}
