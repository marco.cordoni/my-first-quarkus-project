package client;

import dto.Isbn13;
import io.quarkus.test.Mock;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import rest.client.NumberProxy;

@Mock
@RestClient
public class MockNumberProxy implements NumberProxy {

    @Override
    public Isbn13 generateIsbnNumbers() {
        Isbn13 isbn13 = new Isbn13();
        isbn13.setIsbn13("13-003");
        return isbn13;
    }
}
