package controller;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.startsWith;

@QuarkusTest
public class BookControllerIntegrationTest {

    @Test
    public void testGetAllBooks() {
        given()
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
        .when()
                .get("/api/books")
        .then()
                .statusCode(200)
                .body("size()",is(2));
    }

    @Test
    public void testCountAllBooks() {
        given()
                .header(HttpHeaders.ACCEPT, MediaType.TEXT_PLAIN)
        .when()
                .get("/api/books/count")
        .then()
                .statusCode(200)
                .body(is("2"));
    }

    @Test
    public void testFindBookById() {
        given()
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .pathParam("id", "1")
        .when()
                .get("/api/books/{id}")
        .then()
                .statusCode(200)
                .body("title", is("Vinland Saga"))
                .body("author", is("Makoto Yukimura"));
    }

    @Test
    public void testCreateABook() {
        given()
                .formParam("title", "La profezia dell'armadillo")
                .formParam("author", "Zerocalcare")
                .formParam("year", 2017)
                .formParam("genre", "Comedy")
        .when()
                .post("/api/books")
        .then()
                .statusCode(201)
                .body("isbn_13", startsWith("13-"));
    }

}
