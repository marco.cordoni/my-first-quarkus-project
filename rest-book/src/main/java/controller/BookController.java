package controller;

import dto.BookDto;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.faulttolerance.Fallback;
import org.eclipse.microprofile.faulttolerance.Retry;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.logging.Logger;
import service.BookService;

import java.util.List;
import java.util.Optional;

@Path("/api/books")
@Tag(name = "Book REST endpoint")
@Produces(MediaType.APPLICATION_JSON)
public class BookController {

    @Inject
    private Logger logger;

    @Inject
    private BookService bookService;

    @ConfigProperty(name = "env", defaultValue = "unknown")
    private String env;

    // curl http://localhost:8702/api/books
    @GET
    public List<BookDto> getAllBooks() {
        logger.infof("[%s] - Returns all books", env);
        return bookService.getAllBooks();
    }

    @GET
    @Path("/count")
    @Produces(MediaType.TEXT_PLAIN)
    public int countAllBooks() {
        logger.infof("[%s] - Returns the number of books", env);
        return bookService.countAllBooks();
    }

    @GET
    @Path("{id}")
    public Optional<BookDto> findBookById(@PathParam("id") int id) {
        logger.infof("[%s] - Returns the book with id %s", env, id);
        return bookService.findBookById(id);
    }

    // curl -X POST http://localhost:8702/api/books -d "title=Quarkus&author=Marco&yearOfPublication=2023&genre=IT"
    @POST
    @Retry(delay = 1000, maxRetries = 3)
    @Fallback(fallbackMethod = "fallbackOnCreatingABook")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Operation(
            summary = "Creates a book",
            description = "Creates a book with an ISBN number"
    )
    public Response createABook(
            @FormParam("title") String title,
            @FormParam("author") String author,
            @FormParam("yearOfPublication") int yearOfPublication,
            @FormParam("genre") String genre
    ) {
        logger.infof("[%s] - Starting to create the book: %s", env, title);
        BookDto bookDto = bookService.createABook(title, author, yearOfPublication, genre, true);

        logger.infof("[%s] - Created the book: %s", env, bookDto);
        return Response.status(201).entity(bookDto).build();
    }

    public Response fallbackOnCreatingABook(String title, String author, int yearOfPublication, String genre) {
        logger.infof("[%s] - FALLBACK on creating the book: %s", env, title);
        BookDto bookDto = bookService.createABook(title, author, yearOfPublication, genre, false);

        logger.warnf("[%s] - Created the book: %s without ISBN 13", env, bookDto);
        return Response.status(206).entity(bookDto).build();
    }
}
