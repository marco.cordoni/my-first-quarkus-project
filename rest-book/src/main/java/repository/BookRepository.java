package repository;

import dto.BookDto;
import entity.BookEntity;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.PathParam;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class BookRepository {

    public List<BookEntity> getAllBooks() {
        return List.of(
                new BookEntity(1, "13-001", "Vinland Saga", "Makoto Yukimura", 2005, "Storical", Instant.now()),
                new BookEntity(2, "13-002", "My Hero Academia", "Kōhei Horikoshi", 2014, "Action", Instant.now())
        );
    }

    public int countAllBooks() {
        return getAllBooks().size();
    }

    public Optional<BookEntity> findBookById(@PathParam("id") int id) {
        return getAllBooks().stream().filter(book -> book.getId() == id).findFirst();
    }
}
