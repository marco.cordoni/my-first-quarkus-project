package mapper;

import dto.BookDto;
import entity.BookEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "jakarta")
public interface BookMapper {

    BookDto map(BookEntity book);

    BookEntity map(BookDto book);

    List<BookDto> map(List<BookEntity> list);

}
