package service;

import dto.BookDto;
import entity.BookEntity;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import mapper.BookMapper;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import repository.BookRepository;
import rest.client.NumberProxy;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@ApplicationScoped
public class BookService {

    @Inject
    private BookRepository bookRepository;

    @Inject
    private BookMapper bookMapper;

    @Inject
    @RestClient
    private NumberProxy numberProxy;

    public List<BookDto> getAllBooks() {
         return bookMapper.map(bookRepository.getAllBooks());
    }

    public int countAllBooks() {
        return bookRepository.countAllBooks();
    }

    public Optional<BookDto> findBookById(int id) {
        Optional<BookEntity> bookEntityOpt = bookRepository.findBookById(id);
        BookDto bookDto = bookEntityOpt.isPresent() ? bookMapper.map(bookEntityOpt.get()) : null;
        return Optional.ofNullable(bookDto);
    }

    public BookDto createABook(String title, String author, int yearOfPublication, String genre, boolean setIsbn13) {
        BookDto bookDto = new BookDto();
        bookDto.setId(new Random().nextInt());
        bookDto.setTitle(title);
        bookDto.setAuthor(author);
        bookDto.setYearOfPublication(yearOfPublication);
        bookDto.setGenre(genre);
        bookDto.setCreationDate(Instant.now());
        bookDto.setIsbn13(setIsbn13 ? numberProxy.generateIsbnNumbers().getIsbn13() : "Will be set later");

        BookEntity bookEntity = bookMapper.map(bookDto);
        // save the entity on the DB and return the updated entity
        // re-convert the entity in DTO and return it

        return bookDto;
    }

}
